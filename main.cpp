#include <iostream>
#include <armadillo>
#include <cmath>
#include <fstream>
#include <time.h>

using namespace std;
using namespace arma;

int main()
{
    //Initializing vectors, integers and doubles:
    int i;
    int n = 10000;
    double h  = 1.0/(n+1.0);
    mat A = zeros<mat>(n,n);
    vec eps = zeros<vec>(n+2);
    vec x = linspace<vec>(0,1,n+2);
    vec v = zeros<vec>(n+2);
    vec u = 1.0-(1.0-exp(-10.0))*x-exp(-10.0*x);
    vec b = pow(h, 2.0)*100.0*exp(-10.0*x);
    vec borg = b;

    //Filling in the matrix A:
    for(i=0; i < n-1; i++) {
        A(i,i) = 2.0;
        A(i,i+1) = -1.0;
        A(i+1,i) = -1.0;
    }
    A(n-1,n-1) = 2.0;
    mat Aorg = A; //Making a copy.

    clock_t start, finish;
    start = clock();
    //Row reduction:
    for(i=1; i < n; i++) {
        b(i) = b(i) + (1.0/A(i-1,i-1))*b(i-1);
        A(i,i) = A(i,i) - (A(i,i-1)/A(i-1,i-1))*A(i-1,i);
        A(i,i-1) = 0;
    }

    //Solving for v:
    v(n) = b(n)/A(n-1,n-1);
    for(i=n-1; i > 0; i--) {
        v(i) = (b(i) + v(i+1))/A(i,i);
    }
    finish = clock();
    cout << "Time (s) taken for custom algorithm:   " << ( (finish - start)/CLOCKS_PER_SEC ) << endl;



    //Computing the relative error (for the custom algorithm):
    for(i=1;i<n+1;i++) {
        eps(i) = log10(abs((v(i)-u(i))/u(i)));
    }

    //Using armadillo LU to make the same calculation:
    start = clock();
    mat L, U, P;
    lu(L,U,Aorg);
    vec vlu;
    vec vluf;
    solve(vlu, trimatl(L), borg.subvec(1,n));
    solve(vluf, trimatu(U), vlu);
    vec vluffixed = zeros<vec>(n+2);
    finish = clock();
    cout << "Time (s) taken for LU:   " << ( (finish - start)/CLOCKS_PER_SEC ) << endl;

    //Filling the elements of vluf into a larger array so the points match with x:
    for(i=1; i<n+1; i++) {
        vluffixed(i) = vluf(i-1);
    }

    //Computing the relative error of the LU-decomp method:
    vec epslu = zeros(n+2);
    /*for(i=1;i<n+1;i++) {
        cout << abs((vluffixed(i)-u(i))/u(i)) <<endl;
        epslu(i) = log10(abs((vluffixed(i)-u(i))/u(i)));
    }*/
    double max_error_custom = eps.max();
    //double max_error_LU = epslu.max();
    cout << "Relative error with custom algorithm:   " << max_error_custom << endl;
    //cout << "Relative error with LU:   " << max_error_LU << endl;

    //Write the results to files:
    ofstream vresults;
    vresults.open ("resultsv.txt");
    vresults << v ;
    vresults.close();
    ofstream vlufresults;
    vlufresults.open ("resultsvluf.txt");
    vlufresults << vluffixed ;
    vlufresults.close();
    return 0;
}

