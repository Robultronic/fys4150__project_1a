from scitools.std import *
import numpy as np

#Reading vector:
def vecread(filename):
	table = open(filename, 'r')
	rows = table.readlines()
	n = 1
	m = len(rows)
	#print m
	c = 0
	elements = np.zeros(m)
	for i in rows:
		row = i.split(' ')
		row = ''.join(row)
		#row = filter(lambda number: number.strip(), row)
		#print row
		for k in range(len(row)):
			elements[c] = float(row)
		c += 1
	return elements

v = vecread("resultsv.txt")
vluf = vecread("resultsvluf.txt")
print len(v), len(vluf)
x = linspace(0,1,len(v))
u = 1.0 - (1.0 - exp(-10.0))*x - exp(-10.0*x)

#Making and saving plots:
figure(1)
plot(x,v, title = "Custom algorithm vs analytical solution for n = %.f" % (len(v) - 2), xlabel = "x", ylabel = "u(x)", legend = "Custom", hardcopy = "lastcust%.d.png")
hold('on')
plot(x,u, legend = "Analytical")

figure(2)
plot(x,vluf, title = "LU decomp vs analytical solution for n = %.f" % (len(vluf) - 2), xlabel = "x", ylabel = "u(x)", legend = "LU", hardcopy = "lastlun%.f.png" % (len(vluf) - 2))
hold('on')
plot(x,u, legend = "Analytical")
'''
figure(3)
plot(x,u, title = "Only analytical")'''

